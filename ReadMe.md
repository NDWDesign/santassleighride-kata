``
#Santa's Sleigh Ride#

Öffne index.html in einem Browser deiner Wahl.

Die quadratischen Felder stellen eine Höhenkarte dar.
Die Zahlen in den Feldern geben die Höhenwerte der Kachel an.

Der rote, runde Kreis stellt die aktuelle Position von Santa dar.
Die Zahlen in den Kreis geben die Schrittnummerierung an.


##1. Aufgabe##
   
   Bringe Santa sicher zu seinem Ziel
   Implementiere dazu die Methode getNextStep(view) in src/santa.js
   
   Regeln:
   1. Santa startet an der Position startPos.
   2. Santa soll zur Position endPos gelangen.
   3. view gibt den Kartenausschnitt den Santa aktuell sehen kann. 
      Dabei steht er selber im Mittelpunkt des Kartenausschnitts.
   3. Santa darf nicht vom Rand der Karte fallen. 
   4. Santa darf keine Steigungen größer als maxClimbableInclination überwinden
   5. Wenn Santa zu lange unterwegs ist, wird er leider verhungern.





