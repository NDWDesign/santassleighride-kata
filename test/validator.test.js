import Validator from "../src/validator";

const xTerrain = [[2, 0, 3]];
const yTerrain = [[2], [0], [3]];
const flatTerrain = [[0, 0, 0], [0, 0, 0], [0, 0, 0]];

describe('Santas Schrittgröße  ', () => {
    const validator = new Validator([0, 0], [3, 3], 999, 1, 999, flatTerrain,);
    it('darf nicht größer als maxSpeed sein', () => {
        expect(validator.validatePosition([2, 0])).toEqual(false);
    });
    it('soll kleiner gleich maxSpeed sein', () => {
        expect(validator.validatePosition([1, 0])).toEqual(true);
    });
});

describe('Santas Kletterleidenschaft', () => {
    const validator = new Validator([1, 0], [3, 3], 999, 999, 2, xTerrain);
    it("muss überprüft werden", () => {
        expect(validator.validatePosition([2, 0])).toEqual(false);
    });
    it("darf nicht behindert werden", () => {
        expect(validator.validatePosition([0, 0])).toEqual(true);
    });
});

describe('Santa darf nicht vom Rand der Welt fallen', () => {
    it("weder in x-Richtung", () => {
        const validator = new Validator([1, 0], [3, 3], 999, 999, 999, xTerrain);
        expect(validator.validatePosition([-1, 0])).toEqual(false);
        expect(validator.validatePosition([3, 0])).toEqual(false);
        expect(validator.validatePosition([2, 0])).toEqual(true);
        expect(validator.validatePosition([1, 0])).toEqual(true);
    });
    it("noch in y-Richtung", () => {
        const validator = new Validator([0, 1], [3, 3], 999, 999, 999, yTerrain);
        expect(validator.validatePosition([0, -2])).toEqual(false);
        expect(validator.validatePosition([0, 3])).toEqual(false);
        expect(validator.validatePosition([0, 2])).toEqual(true);
        expect(validator.validatePosition([0, 1])).toEqual(true);
    });
});

describe('Santas Ausdauer', () => {
    const validator = new Validator([1, 1], [3, 3], 4, 999, 999, flatTerrain);
    it("wird nicht unterschätzt", () => {
        expect(validator.validatePosition([1, 0])).toEqual(true);
        expect(validator.validatePosition([0, 0])).toEqual(true);
        expect(validator.validatePosition([0, 1])).toEqual(true);
        expect(validator.validatePosition([0, 2])).toEqual(true);
    });
    it("und nicht überschätzt", () => {
        expect(validator.validatePosition([0, 1])).toEqual(false);
    });
});

describe('Santas Ankunft', () => {
    const validator = new Validator([1, 1], [2, 2], 999, 999, 999, flatTerrain);
    it("wird erwartet", () => {
        expect(validator.validatePosition([2, 2])).toEqual(false);
    });
});