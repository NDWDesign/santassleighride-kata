import Radar from "../src/radar";

const terrain = [[1, 2, 3], [4, 5, 6], [7, 8, 9]];

const radar = new Radar(terrain, 3);

describe("Radar am Rand der Karte", () => {
    it("Soll am oberen Rand ein Array mit null-Werten zurück geben", () => {
        expect(radar.getView([1, 0])).toEqual(
            [
                [null, null, null],
                [1, 2, 3],
                [4, 5, 6]
            ]
        );
    });

    it("Soll am unteren Rand ein Array mit null-Werten zurück geben", () => {
        expect(radar.getView([1, 2])).toEqual(
            [
                [4, 5, 6],
                [7, 8, 9],
                [null, null, null]
            ]);
    });

    it("Soll am linken Rand null zurück geben", () => {
        expect(radar.getView([0, 1])).toEqual(
            [
                [null, 1, 2],
                [null, 4, 5],
                [null, 7, 8]
            ]);
    });

    it("Soll am rechten Rand null zurück geben", () => {
        expect(radar.getView([2, 1])).toEqual(
            [
                [2, 3, null],
                [5, 6, null],
                [8, 9, null]
            ]);
    });
});
describe("Radar an den Ecken der Karte", () => {
    it("soll an der linken oberen Ecke null-Werte zurück geben", () => {
        expect(radar.getView([0, 0])).toEqual(
            [
                [null, null, null],
                [null, 1, 2],
                [null, 4, 5]
            ]);
    });
    it("soll an der rechten oberen Ecke null-Werte zurück geben", () => {
        expect(radar.getView([2, 0])).toEqual(
            [
                [null, null, null],
                [2, 3, null],
                [5, 6, null]
            ]);
    });

    it("soll an der rechten unteren Ecke null-Werte zurück geben", () => {
        expect(radar.getView([2, 2])).toEqual(
            [
                [5, 6, null],
                [8, 9, null],
                [null, null, null]
            ]);
    });

    it("soll an der linken unteren Ecke null-Werte zurück geben", () => {
        expect(radar.getView([0, 2])).toEqual(
            [
                [null, 4, 5],
                [null, 7, 8],
                [null, null, null]
            ]);
    });
});