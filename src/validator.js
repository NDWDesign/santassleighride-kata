export default class Validator {
    /**
     * @param startPos: number[] - Santas Startposition. Array der Form [x: number, y: number]
     * @param endPos: number[] - Santas Ziel. Array der Form [x: number, y: number]
     * @param maxSteps: number - Maximale Anzahl von Schritten die Santa zurück legen kann.
     * @param maxSpeed: number - Maximale Anzahl von Felder die Santa in einen Interval gehen kann
     * @param maxClimbableInclination : number - Maximaler Höhenunterschied den Santa bewältigen kann
     * @param terrain: number[][] - Weltkarte
     */
    constructor(startPos, endPos, maxSteps, maxSpeed, maxClimbableInclination, terrain) {
        this.currentPos              = startPos;
        this.endPos                  = endPos;
        this.maxSteps                = maxSteps;
        this.maxSpeed                = maxSpeed;
        this.maxClimbableInclination = maxClimbableInclination;
        this.terrain                 = terrain;
        this.stepCount               = 0;
    }

    /**
     * Überprüft die neue Position von Santa
     *
     * @param newPos : number[] - Array der Form [x: number, y: number]
     * @returns {boolean} - false: Neue Position ist nicht gültig / Santa hat sein Ziel erreicht / .
     */
    validatePosition(newPos) {

        this.stepCount++;
        // Prüfen ob Santa sein Ziel erreicht hat



        // Prüfen ob Santa schon zu lange läuft.
        if (this.stepCount > this.maxSteps) {
            this.showMessage("Santa ist leider auf dem Weg verhungert!");
            return false;
        }
        // Prüfen ob neue Position innerhalb der Weltkarte liegt
        if (
            newPos[0] < 0
            || newPos[0] >= this.terrain[0].length
            || newPos[1] < 0
            || newPos[1] >= this.terrain.length
        ) {
            this.showMessage("Santa ist leider vom Rand der Welt gefallen");
            return false;
        }

        // Prüfen ob Santa versucht zu schummeln.
        const deltaX = Math.abs(this.currentPos[0] - newPos[0]);
        const deltaY = Math.abs(this.currentPos[1] - newPos[1]);

        if (deltaX > this.maxSpeed || deltaY > this.maxSpeed) {
            this.showMessage("Santa kann nicht so große Schritte machen!");
            return false;
        }
        if (Math.abs(
            this.terrain[this.currentPos[1]][this.currentPos[0]] - this.terrain[newPos[1]][newPos[0]]) > this.maxClimbableInclination) {
            this.showMessage("Santa kann nicht so steil klettern!");
            return false;
        }

        if (newPos[0] === this.endPos[0] && newPos[1] === this.endPos[1]) {
            this.showMessage(
                "Santa hat sein Ziel in " + this.stepCount + " Schritten erreicht. Fröhliche Weihnachten!");
            return false;
        }

        this.currentPos = newPos;

        return true;
    }

    showMessage(message) {

        const mapElement = document.getElementById('map');
        if (null === mapElement) {
            console.log(message);
            return;
        }

        const messageElement       = document.createElement('h1');
        messageElement.textContent = message;
        mapElement.appendChild(messageElement);
    }
}