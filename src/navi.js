export default class Navi {
    /**
     * @param startPos: number[] - Die Position auf der Weltkarte bei der Santa seinen Weg beginnt in absolute Koordinaten. Array der Form [x: number, y: number]
     * @param endPos: number[] - Die Position auf der Weltkarte zu der Santa gelangen möchte in absoluten Koordinaten. Array der Form [x: number, y: number]
     * @param maxClimbableInclination: number - Maximale Steigung die Santa bewältigen kann.
     * @param terrain: (number[])[] - Wie Weltkarte als Höhenwerte in x- und y-Richtung
     */
    constructor(startPos, endPos, maxClimbableInclination, terrain) {
    }

    /**
     * Liefert die berechneten Wegpunkte
     * * @returns number[] - Die Richtung in die Santa gehen will. [x: -1...1, y: -1..1]
     */
    getNextStep() {
        return [1, 1];
    }
}