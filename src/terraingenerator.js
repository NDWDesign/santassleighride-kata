export default class TerrainGenerator {
    /**
     * Erzeugt eine Weltkarte anhand der übergebenen Parameter
     *
     * @param startPos : number[] - Startposition, Array der Form [x: number, y: number]
     * @param endPos: number[] - Endposition, Array der Form [x: number, y:number]
     * @param width: number - Breite der zu generierende Weltkarte
     * @param height: number - Höhe der zu generierenden Weltkarte
     * @param maxAltitude: number - Maximale Höhe
     * @param maxInclination : number - Maximale Steigung
     * @param maxClimbableInclination : number - Maximale Steigung die Sanata bewältigen kann.
     *
     * @return number[][]
     */
    generateMap(startPos, endPos, width, height, maxAltitude, maxInclination, maxClimbableInclination) {
        return [
            [0, 3, 3, 1, 1, 2, 3, 2, 2, 1, 1, 1],
            [1, 1, 1, 3, 2, 2, 3, 3, 2, 1, 1, 0],
            [1, 2, 1, 3, 2, 3, 3, 3, 3, 2, 1, 0],
            [2, 4, 4, 2, 4, 2, 4, 5, 4, 3, 2, 1],
            [3, 6, 5, 6, 4, 1, 3, 4, 5, 4, 3, 2],
            [4, 7, 6, 5, 3, 3, 1, 2, 4, 5, 4, 3],
            [5, 8, 7, 6, 5, 4, 1, 2, 4, 5, 6, 5],
            [6, 8, 5, 4, 3, 2, 1, 3, 5, 7, 7, 8],
            [8, 7, 4, 3, 2, 3, 3, 5, 6, 7, 8, 9],
            [8, 6, 3, 2, 1, 0, 2, 4, 5, 7, 9, 10]
        ]
    }
}
