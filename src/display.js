export default class Display {
    /**
     * @param terrain: number[][] - Wie Weltkarte als Höhenwerte in x- und y-Richtung
     */
    constructor(terrain) {
        this.terrain   = terrain;
        this.stepCount = 0;
    }

    /**
     * Erzeugt eine Darstellung der aktuellen Weltkarte
     * @param startPos: number[][] - Startposition, Array der Form [x: number, y: number]
     * @param endPos: number[][] - Endposition, Array der Form [x: number, y: number]
     */
    createMapView(startPos, endPos) {
        const mapElement = document.getElementById("map");
        this.terrain.forEach((row, rowIndex) => {
            let rowElement = document.createElement("div");
            mapElement.appendChild(rowElement);
            row.forEach((height, columnIndex) => {
                let cellElement         = document.createElement("div");
                cellElement.id          = columnIndex + "-" + rowIndex;
                cellElement.className   = "height-" + height;
                cellElement.textContent = height;
                if (startPos[0] === columnIndex && startPos[1] === rowIndex) {
                    cellElement.className += " startPos";
                } else if (endPos[0] === columnIndex && endPos[1] === rowIndex) {
                    cellElement.className += " endPos";
                }
                rowElement.appendChild(cellElement);
            });
        })
    };

    /**
     * Zeigt Santas aktuelle Position auf der Weltkarte an.
     * @param currentPos: number[] - Array der Form [x: number, y: number]
     */
    showPosition(currentPos = [0, 0]) {
        let cellElement = document.getElementById(currentPos[0] + "-" + currentPos[1])

        // Ungültige Positionen nicht darstellen.
        if (null === cellElement) {
            return;
        }
        let stepElement       = document.createElement("div");
        stepElement.className = "step";
        stepElement.innerText = this.stepCount.toString();
        cellElement.appendChild(stepElement);

        this.stepCount++;
    }
}

