export default class Radar {
    /**
     * @param terrain: (number[])[] - Wie Weltkarte als Höhenwerte in x- und y-Richtung
     * @param viewSize: number - Kantenlänge des Ausschnitts der Weltkarte der berechnet werden soll.
     */
    constructor(terrain, viewSize = 3) {
        this.terrain   = terrain;
        this.viewSize  = viewSize;
        this.mapHeight = terrain.length;
        this.mapWidth  = terrain[0].length;
    }

    /**
     * Liefert den aktuellen Auschnitt der Weltkarte den Santa sehen kann.
     *
     * @param position : number[] - Aktuelle Position als absolute Koordinaten auf der Weltkarte
     * @returns {number[][]} - Ausschnitt der Weltkarte den Santa aktuell sehen kann.
     */
    getView(position) {

        let left   = position[0] - 1;
        let right  = position[0] + 1;
        let top    = position[1] - 1;
        let bottom = position[1] + 1;

        let view   = [];
        for (let y = top; y <= bottom; y++) {
            if (y < 0
                || y >= this.mapHeight) {
                view.push(new Array(this.viewSize).fill(null));
            } else {
                let row = [];
                for (let x = left; x <= right; x++) {
                    if (x < 0 || x >= this.mapWidth) {
                        row.push(null);
                    } else {
                        row.push(this.terrain[y][x]);
                    }
                }
                view.push(row);
            }
        }
        return view;
    }
}