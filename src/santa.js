export default class Santa {


    /**
     * @param startPos: number[] - Die Position auf der Weltkarte bei der Santa seinen Weg beginnt in absolute Koordinaten. Array der Form [x: number, y: number]
     * @param endPos: number[] - Die Position auf der Weltkarte zu der Santa gelangen möchte in absoluten Koordinaten. Array der Form [x: number, y: number]
     * @param maxClimbableInclination: number - Maximale Steigung die Santa bewältigen kann.
     */
    constructor(startPos, endPos, maxClimbableInclination) {
    }

    /**
     * Liefert die relativen Koordinaten in die sich Santa bewegen soll
     *
     * @param view: number[][] - Der Ausschnitt der Welt den Santa aktuell sehen kann. Santa befindet sich in der Mitte. z.B:
     * [[1, 3, 3]
     * ,[2, 4, 1]
     * ,[3, 1, 2]]
     * @returns number[] - Die Richtung in die Santa gehen will. [x: -1...1, y: -1..1]
     */
    getNextStep(view) {

        return [Math.round((Math.random() * 2 - 1)), Math.round((Math.random() * 2 - 1))];
    }
}