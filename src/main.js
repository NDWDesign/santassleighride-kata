import Santa from "./santa.js";
import Radar from "./radar.js";
import Display from "./display.js";
import Validator from "./validator.js";
import TerrainGenerator from "./terraingenerator.js";
import Navi from "./navi.js";

class GameEngine {
    constructor(terrain, startPos, endPos, maxSpeed, maxSteps, maxClimbableInclination, delay, wayFinder) {
        this.currentPos = startPos;
        this.delay      = delay;
        this.wayFinder  = wayFinder;
        this.display    = new Display(terrain);
        this.radar      = new Radar(terrain);
        this.validator  = new Validator(startPos, endPos, maxSteps, maxSpeed, maxClimbableInclination, terrain);

        // Anzeige initialisieren
        this.display.createMapView(startPos, endPos);
        this.display.showPosition(this.currentPos);
    }

    /**
     * Führt einen Schritt in der Spielberechnung durch.
     */
    play = () => {

        // Santa fragen wohin er gehen möchte.
        const direction = this.wayFinder.getNextStep(
            this.radar.getView(this.currentPos)
        );

        // Neue Position berechnen.
        this.currentPos = [this.currentPos[0] + direction[0], this.currentPos[1] + direction[1]];

        this.display.showPosition(
            this.currentPos
        );

        // Prüfen ob Spiel fortgeführt werden kann.
        if (!this.validator.validatePosition(this.currentPos)) {
            return;
        }

        window.setTimeout(this.play, this.delay);
    }
}

const startPos                = [0, 0],
      endPos                  = [5, 9],
      maxClimbableInclination = 1;

const terrainGenerator = new TerrainGenerator();
const terrain          = terrainGenerator.generateMap(startPos, endPos, 10, 10, 10, 3, maxClimbableInclination);

const
    santa = new Santa(startPos, endPos, maxClimbableInclination),
    navi  = new Navi(startPos, endPos, maxClimbableInclination, terrain);

const gameEngine = new GameEngine(
    terrain,
    startPos,
    endPos,
    1,
    200,
    maxClimbableInclination,
    100,
    santa
);

gameEngine.play();
